///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Arthur Lee <leea3@hawaii.edu>
/// @date    20 Jan 2022 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n");
   
   /*using const int limits the scope to main() and also it is typechecked */
   const int DEFAULT_MAX_NUMBER = 2048;
   int theMaxValue;

   /*Checks the number of arguments entered while compiling and decides whether or not to prompt the user for a number */
   if(argc > 1){
      theMaxValue = atoi( argv[1]);
      if(theMaxValue < 1){
         printf("Entered max value must be an integer greater than or equal to 1, exiting...\n");
         return 1;
      }
   }
   else {
      theMaxValue = DEFAULT_MAX_NUMBER;
   }
   
   
   
   /*adding 1 to theMaxValue will ensure the modulus operator gives a value between 1 and theMaxValue*/
   srand(time(0));
   int TARGET_NUMBER = rand() % (theMaxValue) + 1;

   //printf("Debugging: TARGET_NUMBER = %i\n", TARGET_NUMBER);

   int aGuess;
   do {
      printf("Ok cat, I'm thinking of a number from 1 to %i. Make a guess; ", theMaxValue);
      scanf("%i", &aGuess);

      if( (aGuess > TARGET_NUMBER) & (aGuess >= 1) & (aGuess <= theMaxValue) ){
         printf("No cat... the number I'm thinking of is smaller than %i\n", aGuess);
      }
      if( (aGuess < TARGET_NUMBER) & (aGuess >= 1) & (aGuess <= theMaxValue) ){
         printf("No cat... the number I'm thinking of is larger than %i\n", aGuess);
      }
      if(aGuess > theMaxValue) {
         printf("You must enter a value less than %d\n", theMaxValue + 1);
      }
      if(aGuess < 1) {
         printf("You must enter a value greater than 0\n");
      }
   } while(aGuess != TARGET_NUMBER);

   printf("You got me.\n");
   
   printf("      |\\      _,,,---,,_\nZZZzz /,`.-'`'    -.  ;-;;,_\n     |,4-  ) )-,_. ,\\ (  `'-'\n    '---''(_/--'  `-'\\_)  \n");

   return 0;
}

